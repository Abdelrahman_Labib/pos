@extends('Admin.layouts.app')
@section('title') <title>{{__('admin.stockItemsSideBar')}}</title> @endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/admin/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin.addButton')}} {{__('admin.stockItemsSideBar')}}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form method="post" action="{{route('admin.stock_items.store')}}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.categoryAttribute')}} <span class="text text-danger">*</span></label>
                                <select class="form-control" name="stock_category_id">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'stock_category_id'])

                            <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.barcodeAttribute')}}<a href="#" id="generate">[{{__('admin.generateAttribute')}}]</a></label>
                                <input type="text" id="generateBarcode" name="barcode" class="form-control" placeholder="{{__('admin.barcodeAttribute')}}" value="{{old('barcode')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'barcode'])

                            <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.nameAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{old('name')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'name'])
                        </div><!-- row -->

                        <div class="row mt-3">
                            <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.sellingAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="number" name="sell_price" class="form-control" placeholder="{{__('admin.sellingAttribute')}}" value="{{old('sell_price')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'sell_price'])

                            <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.purchaseAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="number" name="purchase_price" class="form-control" placeholder="{{__('admin.purchaseAttribute')}}" value="{{old('purchase_price')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'purchase_price'])

                            <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.quantityAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="number" name="quantity" class="form-control" placeholder="{{__('admin.quantityAttribute')}}" value="{{old('quantity')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'quantity'])

                            <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.notifyLimitAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="number" name="notification_limit" class="form-control" placeholder="{{__('admin.notifyLimitAttribute')}}" value="{{old('notification_limit')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'notification_limit'])
                        </div><!-- row -->

                        <div class="row mt-3">
                            <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.notesAttribute')}} <span class="text text-danger">*</span></label>
                                <textarea class="form-control" name="notes" rows="3" placeholder="{{__('admin.notesAttribute')}}">{{old('notes')}}</textarea>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'notes'])
                        </div><!-- row -->

                        <button type="submit" class="btn btn-primary btn-sm mt-4">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{__('admin.stockItemsSideBar')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('admin.categoryAttribute')}}</th>
                                    <th>{{__('admin.barcodeAttribute')}}</th>
                                    <th>{{__('admin.nameAttribute')}}</th>
                                    <th>{{__('admin.sellingAttribute')}}</th>
                                    <th>{{__('admin.purchaseAttribute')}}</th>
                                    <th>{{__('admin.quantityAttribute')}}</th>
                                    <th>{{__('admin.notifyLimitAttribute')}}</th>
                                    <th>{{__('admin.notesAttribute')}}</th>
                                    <th>{{__('admin.operationsAttribute')}}</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- editButton -->
    <div class="modal fade" id="editButton">
        <div class="modal-dialog">
            <form class="modal-content" id="edit_form" method="post">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.categoryAttribute')}}</label>
                            <select class="form-control" id="stock_category_id" name="stock_category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'stock_category_id'])

                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.sellingAttribute')}}</label>
                            <input type="number" id="edit_sell_price" name="sell_price" class="form-control" placeholder="{{__('admin.sellingAttribute')}}" value="{{old('sell_price')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'sell_price'])
                    </div><!-- row -->

                    <div class="row mt-3">
                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.barcodeAttribute')}}<a href="#" id="edit_generate">[{{__('admin.generateAttribute')}}]</a></label>
                            <input type="text" id="editGenerateBarcode" name="barcode" class="form-control" placeholder="{{__('admin.barcodeAttribute')}}" value="{{old('barcode')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'barcode'])

                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.purchaseAttribute')}}</label>
                            <input type="number" id="edit_purchase_price" name="purchase_price" class="form-control" placeholder="{{__('admin.purchaseAttribute')}}" value="{{old('purchase_price')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'purchase_price'])
                    </div><!-- row -->
                    <div class="row mt-3">
                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.nameAttribute')}}</label>
                            <input type="text" id="edit_name" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{old('name')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])

                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.quantityAttribute')}}</label>
                            <input type="number" id="edit_quantity" name="quantity" class="form-control" placeholder="{{__('admin.quantityAttribute')}}" value="{{old('quantity')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'quantity'])
                    </div><!-- row -->
                    <div class="row mt-3">
                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.notifyLimitAttribute')}}</label>
                            <input type="number" id="edit_notification_limit" name="notification_limit" class="form-control" placeholder="{{__('admin.notifyLimitAttribute')}}" value="{{old('notification_limit')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notification_limit'])

                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.notesAttribute')}}</label>
                            <textarea class="form-control" id="edit_notes"  name="notes" rows="3" placeholder="{{__('admin.notesAttribute')}}">{{old('notes')}}</textarea>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notes'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-primary btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- deleteButton -->
    <div class="modal fade" id="deleteButton">
        <div class="modal-dialog">
            <form class="modal-content" method="post" id="delete_form">
                @csrf
                @method('delete')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.deleteButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-12 form-control-label" style="font-weight: bold">
                            {{__('admin.deleteMessage')}} <label style="color: #bd2130" id="delete_name"></label>
                        </label>
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('script')
    @include('Admin.layouts.message')
    <!-- DataTables  & Plugins -->
    <script src="/admin/datatables/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="/admin/datatables/jszip/jszip.min.js"></script>
    <script src="/admin/datatables/pdfmake/pdfmake.min.js"></script>
    <script src="/admin/datatables/pdfmake/vfs_fonts.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "buttons": ["copy", "csv", "excel", "pdf", "print"],
                "ajax": {
                    url: "/api/stock_items",
                },
                "columns": [
                    { data: 'id' },
                    { data: 'category_name' },
                    { data: 'barcode' },
                    { data: 'name' },
                    { data: 'sell_price' },
                    { data: 'purchase_price' },
                    { data: 'quantity' },
                    { data: 'notification_limit' },
                    { data: 'notes' },
                    { data: "notes", render: function (data, type, row) {
                            return  "<button title='{{__("admin.editButton")}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                                '<i class="fa fa-edit"></i>' +
                                '</button>'+
                                '<button title="{{__('admin.deleteButton')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+JSON.stringify(row.id)+') >' +
                                '<i class="fa fa-trash"></i>' +
                                '</button>'
                        }
                    },
                ]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        $('#generate').on('click', function (e) {
            e.preventDefault();
            var generate = Math.floor(10000000 + Math.random() * 90000000);
            $('#generateBarcode').val(generate);
        });

        $('#edit_generate').on('click', function (e) {
            e.preventDefault();
            var generate = Math.floor(10000000 + Math.random() * 90000000);
            $('#editGenerateBarcode').val(generate);
        });

        function openModalEdit(item) {
            $('#stock_category_id').val(item.stock_category_id);
            $('#edit_sell_price').val(item.sell_price);
            $('#editGenerateBarcode').val(item.barcode);
            $('#edit_purchase_price').val(item.purchase_price);
            $('#edit_name').val(item.name);
            $('#edit_quantity').val(item.quantity);
            $('#edit_notification_limit').val(item.notification_limit);
            $('#edit_notes').val(item.notes);
            $('#edit_form').attr('action', '/pos/stock_items/' + item.id);
            $('#editButton').modal('show');
        }

        function openModalDelete(item_id) {
            $('#delete_name').text(item_id);
            $('#delete_form').attr('action', '/pos/stock_items/' + item_id);
            $('#deleteButton').modal('show');
        }
    </script>
@endsection
