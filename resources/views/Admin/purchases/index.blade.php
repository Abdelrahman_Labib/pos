@extends('Admin.layouts.app')
@section('title') <title>{{__('admin.purchasesSideBar')}}</title> @endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/admin/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="/admin/select2/css/select2.min.css">
    <link rel="stylesheet" href="/admin/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">{{__('admin.purchasesSideBar')}}</li>
                    </ol>
                </div>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-sm btn-primary float-sm-right" data-toggle="modal" data-target="#addButton" data-backdrop="static" data-keyboard="false">
                        <i class="fa fa-plus"></i> {{__('admin.addButton')}}
                    </button>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin.salesSideBar')}}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form method="post" action="{{route('admin.purchases.store')}}">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-2 mg-t-10 mg-sm-t-0">
                                        <label class="form-control-label">{{__('admin.supplierAttribute')}}</label>
                                        <select class="form-control select2" name="supplier_id" style="width: 100%;">
                                            <option selected disabled>{{__('admin.selectListAttribute')}}</option>
                                            @foreach($suppliers as $supplier)
                                                <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @include('Admin.layouts.error', ['input' => 'supplier_id'])

                                    <div class="col-md-3">
                                        <label class="form-control-label">{{__('admin.nameAttribute')}} / {{__('admin.barcodeAttribute')}}</label>
                                        <input type="text" id="stock_item_id0" name="stock_item_id[]" class="form-control" autocomplete="off" oninput="stockItem(0)" placeholder="{{__('admin.nameAttribute')}} / {{__('admin.barcodeAttribute')}}" required>
                                        <div class="list-group" id="item_list0" onclick="itemList(0)"></div>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'stock_item_id'])

                                    <div class="col-md-2">
                                        <label class="form-control-label">{{__('admin.quantityAttribute')}}</label>
                                        <input type="number" step="0.01" name="quantity[]" class="form-control" placeholder="{{__('admin.quantityAttribute')}}" required>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'quantity'])

                                    <div class="col-md-2">
                                        <label class="form-control-label">{{__('admin.purchaseAttribute')}}</label>
                                        <input type="number" step="0.01" name="purchase[]" class="form-control" placeholder="{{__('admin.purchaseAttribute')}}" required>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'purchase'])

                                    <div class="col-sm-2 mg-t-10 mg-sm-t-0">
                                        <label class="form-control-label">{{__('admin.dateAttribute')}}</label>
                                        <input type="date" class="form-control" name="date" required>
                                    </div>
                                    @include('Admin.layouts.error', ['input' => 'date'])

                                    <div class="col-md-1" style="margin-top: 31px">
                                        <button class="add_field_button btn btn-block btn-primary"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div><!-- row -->

                            </form>

                            <table class="table table-bordered table-striped mt-4" id="myTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('admin.nameAttribute')}} / {{__('admin.barcodeAttribute')}}</th>
                                    <th>{{__('admin.purchaseAttribute')}}</th>
                                    <th>{{__('admin.quantityAttribute')}}</th>
                                    <th>{{__('admin.sellingAttribute')}}</th>
                                    <th>{{__('admin.totalAttribute')}}</th>
                                    <th>{{__('admin.operationsAttribute')}}</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div><!-- row -->

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- deleteButton -->
    <div class="modal fade" id="deleteButton">
        <div class="modal-dialog">
            <form class="modal-content" method="post" id="delete_form">
                @csrf
                @method('delete')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.deleteButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-12 form-control-label" style="font-weight: bold">
                            {{__('admin.deleteMessage')}} <label style="color: #bd2130" id="delete_name"></label>
                        </label>
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('script')
    @include('Admin.layouts.message')
    <!-- DataTables  & Plugins -->
    <script src="/admin/datatables/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="/admin/datatables/jszip/jszip.min.js"></script>
    <script src="/admin/datatables/pdfmake/pdfmake.min.js"></script>
    <script src="/admin/datatables/pdfmake/vfs_fonts.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- Select2 -->
    <script src="/admin/select2/js/select2.full.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            var wrapper = $(".input_wrap");
            var add_button = $(".add_field_button");
            var i = 1;

            $(add_button).click(function (e) {
                e.preventDefault();
                $(wrapper).append('<div class="row all_divs">' +
                    '<div class="col-md-5" style="margin-top: 10px">' +
                        '<input type="text" id="stock_item_id'+i+'" name="stock_item_id[]" autocomplete="off" class="form-control" oninput="stockItem('+i+')" placeholder="{{__('admin.nameAttribute')}} / {{__('admin.barcodeAttribute')}}" required>'+
                        '<div class="list-group" id="item_list'+i+'" onclick="itemList('+i+')"></div>'+
                    '</div>' +
                    '<div class="col-md-3" style="margin-top: 10px">' +
                        '<input type="number" step="0.01" name="quantity[]" class="form-control" placeholder="{{__('admin.quantityAttribute')}}" required>' +
                    '</div>' +
                    '<div class="col-md-3" style="margin-top: 10px">' +
                        '<input type="number" step="0.01" name="purchase[]" class="form-control" placeholder="{{__('admin.purchaseAttribute')}}" required>' +
                    '</div>' +
                    '<div class="col-md-1" style="margin-top: 10px">' +
                    '<button class="btn btn-danger remove_field"><i class="fa fa-minus-circle"></i></button>' +
                    '</div>' +
                    '</div>'); //add input box
                i++;
            });

            $(document).on("click",".remove_field",function(){
                $(this).closest('.all_divs').remove();
            });

            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "buttons": ["csv", "excel", "pdf", "print"],
                "ajax": {
                    url: "/api/purchases",
                },
                "columns": [
                    { data: 'id' },
                    { data: 'supplier_name' },
                    { data: 'item_name' },
                    { data: 'quantity' },
                    { data: 'purchase' },
                    { data: 'date' },
                    { data: "date", render: function (data, type, row) {
                            return '<button title="{{__('admin.deleteButton')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+JSON.stringify(row.supplier_id)+') >' +
                                '<i class="fa fa-trash"></i>' +
                                '</button>'
                        }
                    },
                ]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        function stockItem(counter)
        {
            var inputVal = document.getElementById('stock_item_id'+counter).value;
            if(inputVal != ''){
                $.get("/api/items_ajax",{ search: inputVal }, function (data) {
                    $('#item_list'+counter).empty();
                    $.each(data, function (i, item) {
                        $('#item_list'+counter).append('<button type="button" class="list-group-item list-group-item-action">'+item.name +'/'+ item.barcode +'</button>');
                    });
                });
            }
            $('#item_list'+counter).empty();
        }

        function itemList(counter)
        {
            var content= $('#item_list'+counter).children().text();
            $('#stock_item_id'+counter).val(content);
            $('#item_list'+counter).empty();
        }

        function openModalDelete(purchase_id) {
            $('#delete_name').text(purchase_id);
            $('#delete_form').attr('action', '/pos/purchases/' + purchase_id);
            $('#deleteButton').modal('show');
        }
    </script>
@endsection
