@extends('Admin.layouts.app')
@section('title') <title>{{__('admin.suppliersSideBar')}}</title> @endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/admin/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin.addButton')}} {{__('admin.suppliersSideBar')}}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form method="post" action="{{route('admin.suppliers.store')}}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.nameAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" value="{{old('name')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'name'])

                            <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.contactAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="text" name="contact" class="form-control" placeholder="{{__('admin.contactAttribute')}}" value="{{old('contact')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'contact'])
                        </div><!-- row -->

                        <div class="row mt-3">
                            <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.addressAttribute')}} <span class="text text-danger">*</span></label>
                                <input type="text" name="address" class="form-control" placeholder="{{__('admin.addressAttribute')}}" value="{{old('address')}}" required>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'address'])

                            <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.notesAttribute')}} <span class="text text-danger">*</span></label>
                                <textarea class="form-control" name="notes" rows="3" placeholder="{{__('admin.notesAttribute')}}">{{old('notes')}}</textarea>
                            </div>
                            @include('Admin.layouts.error', ['input' => 'notes'])
                        </div><!-- row -->

                        <button type="submit" class="btn btn-primary btn-sm mt-4">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{__('admin.suppliersSideBar')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('admin.nameAttribute')}}</th>
                                    <th>{{__('admin.contactAttribute')}}</th>
                                    <th>{{__('admin.addressAttribute')}}</th>
                                    <th>{{__('admin.notesAttribute')}}</th>
                                    <th>{{__('admin.operationsAttribute')}}</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- editButton -->
    <div class="modal fade" id="editButton">
        <div class="modal-dialog">
            <form class="modal-content" id="edit_form" method="post">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.nameAttribute')}}</label>
                            <input type="text" id="edit_name" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])

                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.contactAttribute')}}</label>
                            <input type="text" id="edit_contact" name="contact" class="form-control" placeholder="{{__('admin.contactAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'contact'])
                    </div><!-- row -->

                    <div class="row mt-3">
                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.addressAttribute')}}</label>
                            <input type="text" id="edit_address" name="address" class="form-control" placeholder="{{__('admin.addressAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'address'])

                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <label class="form-control-label">{{__('admin.notesAttribute')}}</label>
                            <textarea class="form-control" id="edit_notes" name="notes" rows="3" placeholder="{{__('admin.notesAttribute')}}"></textarea>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'notes'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-primary btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- deleteButton -->
    <div class="modal fade" id="deleteButton">
        <div class="modal-dialog">
            <form class="modal-content" method="post" id="delete_form">
                @csrf
                @method('delete')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.deleteButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-12 form-control-label" style="font-weight: bold">
                            {{__('admin.deleteMessage')}} <label style="color: #bd2130" id="delete_name"></label>
                        </label>
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('script')
    @include('Admin.layouts.message')
    <!-- DataTables  & Plugins -->
    <script src="/admin/datatables/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="/admin/datatables/jszip/jszip.min.js"></script>
    <script src="/admin/datatables/pdfmake/pdfmake.min.js"></script>
    <script src="/admin/datatables/pdfmake/vfs_fonts.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
                "buttons": ["csv", "excel", "pdf", "print"],
                "ajax": {
                    url: "/api/suppliers",
                },
                "columns": [
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'contact' },
                    { data: 'address' },
                    { data: 'notes' },
                    { data: "notes", render: function (data, type, row) {
                            return  "<button title='{{__("admin.editButton")}}' class='btn btn-warning btn-sm btn-condensed' onclick='openModalEdit("+JSON.stringify(row)+")' >" +
                                '<i class="fa fa-edit"></i>' +
                                '</button>'+
                                '<button title="{{__('admin.deleteButton')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+JSON.stringify(row)+') >' +
                                '<i class="fa fa-trash"></i>' +
                                '</button>'
                        }
                    },
                ]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        function openModalEdit(supplier) {
            $('#edit_name').val(supplier.name);
            $('#edit_contact').val(supplier.contact);
            $('#edit_address').val(supplier.address);
            $('#edit_notes').val(supplier.notes);
            $('#edit_form').attr('action', '/pos/suppliers/' + supplier.id);
            $('#editButton').modal({show: true, backdrop: 'static', keyboard: false});
        }

        function openModalDelete(supplier) {
            $('#delete_name').text(supplier.name);
            $('#delete_form').attr('action', '/pos/suppliers/' + supplier.id);
            $('#deleteButton').modal('show');
        }
    </script>
@endsection
