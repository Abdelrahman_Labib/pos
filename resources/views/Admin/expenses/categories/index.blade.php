@extends('Admin.layouts.app')
@section('title') <title>{{__('admin.expenseCategorySideBar')}}</title> @endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/admin/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{__('admin.addButton')}} {{__('admin.expenseCategorySideBar')}}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form method="post" action="{{route('admin.expense_categories.store')}}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-4 mg-t-10 mg-sm-t-0">
                                <label class="form-control-label">{{__('admin.nameAttribute')}}</label>
                                <input type="text" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                            </div>

                            <div class="col-sm-1" style="margin-top: 35px">
                                <button type="submit" class=" btn btn-primary btn-sm">
                                    <i class="fa fa-plus"></i>
                                    {{__('admin.saveButton')}}
                                </button>
                            </div>
                        </div><!-- row -->
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{__('admin.categoriesSideBar')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('admin.nameAttribute')}}</th>
                                    <th>{{__('admin.operationsAttribute')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-warning" title="{{__('admin.editButton')}}" onclick="openModalEdit({{$category}})">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-sm btn-danger" title="{{__('admin.deleteButton')}}" onclick="openModalDelete({{$category}})">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <!-- editButton -->
    <div class="modal fade" id="editButton">
        <div class="modal-dialog">
            <form class="modal-content" id="edit_form" method="post">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.addButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-2 form-control-label">{{__('admin.nameAttribute')}}</label>
                        <div class="col-sm-10 mg-t-10 mg-sm-t-0">
                            <input type="text" id="edit_name" name="name" class="form-control" placeholder="{{__('admin.nameAttribute')}}" required>
                        </div>
                        @include('Admin.layouts.error', ['input' => 'name'])
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-primary btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- deleteButton -->
    <div class="modal fade" id="deleteButton">
        <div class="modal-dialog">
            <form class="modal-content" method="post" id="delete_form">
                @csrf
                @method('delete')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.deleteButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-12 form-control-label" style="font-weight: bold">
                            {{__('admin.deleteMessage')}} <label style="color: #bd2130" id="delete_name"></label>
                        </label>
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('script')
    @include('Admin.layouts.message')
    <!-- DataTables  & Plugins -->
    <script src="/admin/datatables/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="/admin/datatables/jszip/jszip.min.js"></script>
    <script src="/admin/datatables/pdfmake/pdfmake.min.js"></script>
    <script src="/admin/datatables/pdfmake/vfs_fonts.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": true,
            });
        });

        function openModalEdit(category) {
            $('#edit_name').val(category.name);
            $('#edit_form').attr('action', '/pos/expense_categories/' + category.id);
            $('#editButton').modal('show');
        }

        function openModalDelete(category) {
            $('#delete_name').text(category.name);
            $('#delete_form').attr('action', '/pos/expense_categories/' + category.id);
            $('#deleteButton').modal('show');
        }
    </script>
@endsection
