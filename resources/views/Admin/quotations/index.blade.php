@extends('Admin.layouts.app')
@section('title') <title>{{__('admin.quotationsSideBar')}}</title> @endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="/admin/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="/admin/datatables/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">{{__('admin.quotationsSideBar')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{__('admin.quotationsSideBar')}}</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="text" id="stock_item_id" name="stock_item_id[]" class="form-control" autocomplete="off" oninput="stockItem()" placeholder="{{__('admin.nameAttribute')}} / {{__('admin.barcodeAttribute')}}" required>
                                            <div class="list-group" id="item_list"></div>
                                            <div style="color: grey; font-size: 14px" id="hintQuality"></div>
                                        </div>
                                        @include('admin.layouts.error', ['input' => 'stock_item_id'])
                                        <div class="col-md-3">
                                            <input type="number" id="quantity" step="0.01" name="quantity[]" class="form-control" placeholder="{{__('admin.quantityAttribute')}}" required>
                                        </div>
                                        @include('admin.layouts.error', ['input' => 'quantity'])
                                        <div class="col-md-3">
                                            <input type="number" id="sell_price" step="0.01" name="sell_price[]" class="form-control" placeholder="{{__('admin.sellingAttribute')}}" required>
                                        </div>
                                        @include('admin.layouts.error', ['input' => 'sell_price'])

                                        <input type="hidden" id="purchase_price">

                                        <div class="col-md-1">
                                            <button class="add_field_button btn btn-block btn-primary"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                    <table class="table table-bordered table-striped mt-4" id="myTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{__('admin.nameAttribute')}} / {{__('admin.barcodeAttribute')}}</th>
                                            <th>{{__('admin.purchaseAttribute')}}</th>
                                            <th>{{__('admin.quantityAttribute')}}</th>
                                            <th>{{__('admin.sellingAttribute')}}</th>
                                            <th>{{__('admin.totalAttribute')}}</th>
                                            <th>{{__('admin.operationsAttribute')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>

                                <div class="col-sm-4 border border-light" >
                                    <div class="row mt-1">
                                        <div class="col-md-6">
                                            <h5 class="text-center"> {{__('admin.phoneAttribute')}} # </h5>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="customer_contact" class="form-control text-center" autocomplete="off" oninput="phoneCustomer()" placeholder="{{__('admin.phoneAttribute')}} #">
                                            <div class="list-group" id="customer_list" ></div>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <h5 class="text-center"> {{__('admin.customerNameAttribute')}} </h5>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="customer_name" class="form-control text-center" placeholder="{{__('admin.customerNameAttribute')}}">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="text-center"> {{__('admin.subTotalAttribute')}} </h5>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="text-center" id="subtotal"> 0.00 </h5>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="text-center"> {{__('admin.discountAttribute')}} </h5>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="discount" class="form-control isnumber text-center" placeholder="{{__('admin.discountAttribute')}}">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5 class="text-center"> {{__('admin.totalAttribute')}} </h5>
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="text-center" id="grandtotal"> 0.00 </h5>
                                        </div>
                                    </div>

                                    <button type="button" id="btn-process-items" class="btn btn-block btn-primary mt-3"><i class="fa fa-check"></i> {{__('admin.checkoutAttribute')}}  </button>

                                </div>

                            </div><!-- row -->

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


    <!-- deleteButton -->
    <div class="modal fade" id="deleteButton">
        <div class="modal-dialog">
            <form class="modal-content" method="post" id="delete_form">
                @csrf
                @method('delete')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('admin.deleteButton')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <label class="col-sm-12 form-control-label" style="font-weight: bold">
                            {{__('admin.deleteMessage')}} <label style="color: #bd2130" id="delete_name"></label>
                        </label>
                    </div><!-- row -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{__('admin.closeButton')}} <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-info btn-sm">{{__('admin.saveButton')}} <i class="fa fa-check"></i></button>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('script')
    @include('Admin.layouts.message')
    <!-- DataTables  & Plugins -->
    <script src="/admin/datatables/datatables/jquery.dataTables.min.js"></script>
    <script src="/admin/datatables/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/admin/datatables/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="/admin/datatables/jszip/jszip.min.js"></script>
    <script src="/admin/datatables/pdfmake/pdfmake.min.js"></script>
    <script src="/admin/datatables/pdfmake/vfs_fonts.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="/admin/datatables/datatables-buttons/js/buttons.colVis.min.js"></script>

    <script>
        $(function () {

            var add_button = $(".add_field_button");
            var i = 1;
            var total = 0;

            $(add_button).click(function (e) {
                e.preventDefault();
                if($('#stock_item_id').val() != '' && $('#quantity').val() != '' && $('#sell_price').val() != ''){
                    var table = $('#myTable tbody');
                    var inc = 0;

                    table.find("tr").each(function () {
                        var title2 = $(this).find("td:eq(1)").html();
                        var quantity = $(this).find("td:eq(3)").html();

                        if ($('#stock_item_id').val() == title2 ){
                            var newQuantity = parseInt(quantity) + parseInt($('#quantity').val())
                            $(this).find("td:eq(3)").html(newQuantity);
                            $(this).find("td:eq(5)").html(newQuantity * $('#sell_price').val());
                            inc = (inc)+1;
                        }
                        // break the loop once found

                    });
                    total += ($('#quantity').val() * $('#sell_price').val());
                    if(inc == 0) {
                        table.append('<tr>' +
                            '<td>' + i + '</td>' +
                            '<td>' + $('#stock_item_id').val() + '</td>' +
                            '<td>' + $('#purchase_price').val() + '</td>' +
                            '<td>' + $('#quantity').val() + '</td>' +
                            '<td>' + $('#sell_price').val() + '</td>' +
                            '<td>' + $('#quantity').val() * $('#sell_price').val() + '</td>' +
                            '<td>' + '<a href="#" class="remove_field"><i class="fa fa-times"></i></a>' + '</td>' +
                            '</tr>');
                    }
                    $('#stock_item_id').val('');
                    $('#quantity').val('');
                    $('#sell_price').val('');
                    $('#hintQuality').text('');

                    $('#subtotal').text(total);
                    $('#grandtotal').text(total - $('#discount').val());
                }
                i++;
            });

            $(document).on("click",".remove_field",function(e){
                e.preventDefault();
                $(this).closest('tr').remove();
                var getTdChildTotal = $(this).closest('tr').find('td').eq(5).html(); //get td total
                total -= getTdChildTotal; //subtract from variable total
                $('#subtotal').text(total); //push total in id subtotal
                $('#grandtotal').text(total - $('#discount').val());
                i--; //id
            });

            $('#discount').on('input', function () {
                var total = $('#subtotal').text() - $('#discount').val();
                $('#grandtotal').text(total);
            })

            $('#btn-process-items').on('click', function () {

                if($('#customer_contact').val() == '' || $('#customer_name').val() == ''){
                    toastr.options.timeOut = 1000;
                    toastr.error('{{__('admin.enterFieldsMessage')}}')
                }else {
                    //LOOP TABLE TR TD
                    var table = document.getElementById( "myTable" );
                    var tableArr = [];
                    for ( var i = 1; i < table.rows.length; i++ ) {
                        tableArr.push({
                            item_barcode: table.rows[i].cells[1].innerHTML,
                            quantity: table.rows[i].cells[3].innerHTML,
                            sell_price: table.rows[i].cells[4].innerHTML,
                        });
                    }

                    $.post('/pos/quotations',
                        {
                            customer_phone: $('#customer_contact').val(),
                            customer_name:  $('#customer_name').val(),
                            item_array:     tableArr,
                            discount:       $('#discount').val() == '' ? 0 : $('#discount').val(),
                            total:          parseInt($('#grandtotal').text()),
                            _token:         $('meta[name="csrf-token"]').attr('content')
                        }, function (response) {

                            if (response.status === 'success') {
                                window.location.href = '/pos/quotations';

                                var url = '/pos/invoice/INVOICE-' + response.type + '-' +response.invoice_id;
                                window.open(url, '_blank')

                            } else {
                                toastr.options.timeOut = 2000;
                                toastr.error('{{__('admin.wrongFiledMessage')}}')
                            }
                        });
                }
            })

        {{--    $("#example1").DataTable({--}}
        {{--        "responsive": true, "lengthChange": false, "autoWidth": true,--}}
        {{--        "buttons": ["csv", "excel", "pdf", "print"],--}}
        {{--        "ajax": {--}}
        {{--            url: "/api/quotations",--}}
        {{--        },--}}
        {{--        "columns": [--}}
        {{--            { data: 'id' },--}}
        {{--            { data: 'category_name' },--}}
        {{--            { data: 'item_name' },--}}
        {{--            { data: 'item_barcode' },--}}
        {{--            { data: 'quantity' },--}}
        {{--            { data: 'purchase_price' },--}}
        {{--            { data: 'sell_price' },--}}
        {{--            { data: "sell_price", render: function (data, type, row) {--}}
        {{--                    return '<button title="{{__('admin.deleteButton')}}" class="btn btn-danger btn-sm btn-condensed" onclick=openModalDelete('+JSON.stringify(row.id)+') >' +--}}
        {{--                        '<i class="fa fa-trash"></i>' +--}}
        {{--                        '</button>'--}}
        {{--                }--}}
        {{--            },--}}
        {{--        ]--}}
        {{--    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');--}}
        });

        function stockItem()
        {
            var inputVal = document.getElementById('stock_item_id').value;
            if(inputVal != ''){
                $.get("/api/items_ajax",{ search: inputVal }, function (data) {
                    $('#item_list').empty();
                    $.each(data, function (i, item) {
                        $('#item_list').append('<button type="button" id="item_list_details'+i+'" onclick="itemList('+i+')" class="list-group-item list-group-item-action">'+item.name +'/'+ item.barcode +'</button>');
                    });
                });
            }
            $('#item_list').empty();
        }

        function itemList(counter)
        {
            var content= $('#item_list_details' + counter).html();
            var inputVal = content.split('/')[1];
            $('#stock_item_id').val(content);
            $.get("/api/items_ajax",{ search: inputVal }, function (data) {
                $.each(data, function (i, item) {
                    $('#quantity').val(item.quantity);
                    $('#sell_price').val(item.sell_price);
                    $('#purchase_price').val(item.purchase_price);
                });
            });
            $('#item_list').empty();
        }

        function phoneCustomer()
        {
            var inputVal = document.getElementById('customer_contact').value;
            if(inputVal != ''){
                $.get("/api/customers_ajax",{ search: inputVal }, function (data) {
                    $('#customer_list').empty();
                    $.each(data, function (i, customer) {
                        $('#customer_list').append('<button type="button" id="customer_details'+i+'" onclick="phoneList('+i+')" class="list-group-item list-group-item-action">'+customer.name +'/'+ customer.phone +'</button>');
                    });
                });
            }
            $('#customer_list').empty();
        }

        function phoneList(counter)
        {
            var content= $('#customer_details' + counter).html();
            var inputVal = content.split('/');
            $('#customer_name').val(inputVal[0]);
            $('#customer_contact').val(inputVal[1]);
            $('#customer_list').empty();
        }

        function openModalDelete(quotation_id) {
            $('#delete_name').text(quotation_id);
            $('#delete_form').attr('action', '/pos/quotations/' + quotation_id);
            $('#deleteButton').modal('show');
        }
    </script>
@endsection
