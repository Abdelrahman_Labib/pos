<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@yield('title')

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/admin/css/fontawesome-free/css/all.min.css">
    <!-- flag-icon-css -->
    <link rel="stylesheet" href="/admin/css/flag-icon-css/css/flag-icon.min.css">
    <!-- pace-progress -->
    <link rel="stylesheet" href="/admin/pace-progress/themes/black/pace-theme-flat-top.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="/admin/toastr/toastr.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/admin/overlayScrollbars/css/OverlayScrollbars.min.css">
    @if(app()->getLocale() == 'ar')
        <!-- Theme style -->
        <link rel="stylesheet" href="/admin/css/rtl/adminlte.min.css">
        <!-- Bootstrap 4 RTL -->
        <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
        <!-- Custom style for RTL -->
        <link rel="stylesheet" href="/admin/css/rtl/custom.css">
    @else
        <!-- Theme style -->
        <link rel="stylesheet" href="/admin/css/adminlte.min.css">
    @endif
    @yield('css')
</head>
<body class="hold-transition sidebar-mini pace-primary" >
{{--oncontextmenu="return false"--}}
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul @if(app()->getLocale() == 'ar') class="navbar-nav mr-auto-navbav" @else class="navbar-nav ml-auto" @endif>

            <!-- Language -->
            @if(app()->getLocale() == 'ar')
                <a class="pt-2" href="{{ route('admin.locale.setting', 'en') }}">
                    <i class="flag-icon flag-icon-us mr-2"></i>
                </a>
            @else
                <a class="pt-2" href="{{ route('admin.locale.setting', 'ar') }}">
                    <i class="flag-icon flag-icon-om mr-2"></i>
                </a>
            @endif

            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-danger navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{route('admin.dashboard')}}" class="brand-link">
            <img src="/admin/img/pos.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">POS SYSTEM</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{auth()->guard('admin')->user()->image ?: '/admin/img/avatar5.png' }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{auth()->guard('admin')->user()->name}}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{route('admin.dashboard')}}" class="nav-link @if(Route::is('admin.dashboard')) active @endif">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                {{__('admin.dashboardSideBar')}}
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.sales.index')}}" class="nav-link @if(Route::is('admin.sales.index')) active @endif">
                            <i class="nav-icon fas fa-chart-line"></i>
                            <p>
                                {{__('admin.salesSideBar')}}
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.quotations.index')}}" class="nav-link @if(Route::is('admin.quotations.index')) active @endif">
                            <i class="nav-icon fas fa-clipboard"></i>
                            <p>
                                {{__('admin.quotationsSideBar')}}
                            </p>
                        </a>
                    </li>

                    <li class="nav-item @if(Route::is('admin.suppliers.index') || Route::is('admin.purchases.index')) menu-open @endif">
                        <a href="#" class="nav-link @if(Route::is('admin.suppliers.index') || Route::is('admin.purchases.index')) active @endif">
                            <i class="nav-icon fas fa-receipt"></i>
                            <p>
                                {{__('admin.purchasesSideBar')}}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.suppliers.index')}}" class="nav-link @if(Route::is('admin.suppliers.index')) active @endif">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{__('admin.suppliersSideBar')}}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.purchases.index')}}" class="nav-link @if(Route::is('admin.purchases.index')) active @endif">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{__('admin.purchasesSideBar')}}</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item @if(Route::is('admin.stock_categories.index') || Route::is('admin.stock_items.index')) menu-open @endif">
                        <a href="#" class="nav-link @if(Route::is('admin.stock_categories.index') || Route::is('admin.stock_items.index')) active @endif">
                            <i class="nav-icon fas fa-box"></i>
                            <p>
                                {{__('admin.stockSideBar')}}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.stock_categories.index')}}" class="nav-link @if(Route::is('admin.stock_categories.index')) active @endif">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{__('admin.categoriesSideBar')}}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.stock_items.index')}}" class="nav-link @if(Route::is('admin.stock_items.index')) active @endif">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{__('admin.itemsSideBar')}}</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item @if(Route::is('admin.expense_categories.index') || Route::is('admin.expenses.index')) menu-open @endif">
                        <a href="#" class="nav-link @if(Route::is('admin.expense_categories.index') || Route::is('admin.expenses.index')) active @endif">
                            <i class="nav-icon fas fa-hand-holding-usd"></i>
                            <p>
                                {{__('admin.expensesSideBar')}}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.expense_categories.index')}}" class="nav-link @if(Route::is('admin.expense_categories.index')) active @endif">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{__('admin.expenseCategorySideBar')}}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.expenses.index')}}" class="nav-link @if(Route::is('admin.expenses.index')) active @endif">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{__('admin.addButton')}} {{__('admin.expensesSideBar')}}</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.customers.index')}}" class="nav-link @if(Route::is('admin.customers.index')) active @endif">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                {{__('admin.customersSideBar')}}
                            </p>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
       @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/admin/jquery/jquery.min.js"></script>
<!-- Moment -->
<script src="/admin/js/moment/moment.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/admin/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- pace-progress -->
<script src="/admin/pace-progress/pace.min.js"></script>
<!-- Toastr -->
<script src="/admin/toastr/toastr.min.js"></script>
<!-- overlayScrollbars -->
<script src="/admin/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/admin/js/adminlte.js"></script>
{{--<script>--}}
{{--    document.onkeydown = function(e) {--}}
{{--        if(event.keyCode == 123) {--}}
{{--            return false;--}}
{{--        }--}}
{{--        if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {--}}
{{--            return false;--}}
{{--        }--}}
{{--        if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {--}}
{{--            return false;--}}
{{--        }--}}
{{--        if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {--}}
{{--            return false;--}}
{{--        }--}}
{{--        if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {--}}
{{--            return false;--}}
{{--        }--}}
{{--    }--}}
{{--</script>--}}
@yield('script')
</body>
</html>
