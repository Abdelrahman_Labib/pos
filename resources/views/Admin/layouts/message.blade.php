<script>
    $(function() {
        toastr.options.timeOut = 1000;

        if({{Session::has('success') ?: 0}}){
            toastr.success('{{Session::get('success')}}')
        }

        if({{Session::has('error') ?: 0}}){
            toastr.error('{{Session::get('error')}}')
        }

        if({{$errors->any() ?: 0}}){
            toastr.error('{{__('admin.wrongFiledMessage')}}')
        }
    });
</script>
