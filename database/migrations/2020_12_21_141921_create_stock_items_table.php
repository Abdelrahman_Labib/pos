<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('stock_category_id');
            $table->string('name');
            $table->string('barcode')->unique();
            $table->float('sell_price');
            $table->float('purchase_price');
            $table->integer('quantity');
            $table->integer('notification_limit');
            $table->text('notes');
            $table->timestamps();

            $table->foreign('stock_category_id')->references('id')->on('stock_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_items');
    }
}
