<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'AuthController@viewLoginPage')->name('login');
Route::post('login', 'AuthController@authenticate')->name('authenticate');
Route::get('logout', 'AuthController@logout')->name('logout');

Route::middleware(['admin'])->group(function () {
    Route::get('dashboard', 'HomeController@index')->name('dashboard');
    Route::resource('sales', 'SaleController');
    Route::resource('quotations', 'QuotationController');
    Route::resource('suppliers', 'SupplierController');
    Route::resource('purchases', 'PurchaseController');
    Route::resource('stock_categories', 'StockCategoryController');
    Route::resource('stock_items', 'StockItemController');
    Route::resource('expense_categories', 'ExpenseCategoryController');
    Route::resource('expenses', 'ExpenseController');
    Route::resource('customers', 'CustomerController');
    Route::get('invoice/INVOICE-{type}-{id}', 'InvoiceController@index');

    Route::get('set-locale/{locale}', function ($locale) {
        session()->put('locale', $locale);
        return redirect()->back();
    })->name('locale.setting');
});

