<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('sales', 'Api\GetSales');
Route::get('quotations', 'Api\GetQuotations');
Route::get('stock_items', 'Api\GetStockItemDetails');
Route::get('suppliers', 'Api\GetSuppliers');
Route::get('purchases', 'Api\GetPurchases');
Route::get('items_ajax', 'Api\GetItemsAjaxDetails');
Route::get('expenses', 'Api\GetExpenseDetails');
Route::get('customers', 'Api\GetCustomers');
Route::get('customers_ajax', 'Api\GetCustomerAjaxDetails');
