<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetSuppliers extends Controller
{
    public function __invoke(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($filter['value']))? $filter['value'] : false;

        $suppliers =
            DB::table('suppliers')
                ->select([
                    'suppliers.id',
                    'suppliers.name',
                    'suppliers.contact',
                    'suppliers.address',
                    'suppliers.notes',
                ])
                ->groupBy('suppliers.id')
                ->get();

        $total_members = count($suppliers); // get your total no of data;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $suppliers,
        );

        return response()->json($data);
    }
}
