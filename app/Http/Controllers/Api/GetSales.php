<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetSales extends Controller
{
    public function __invoke(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($filter['value']))? $filter['value'] : false;

        $sales =
            DB::table('sales')
                ->select([
                    'sales.id',
                    'stock_categories.name as category_name',
                    'stock_items.name as item_name',
                    'stock_items.barcode as item_barcode',
                    'sales.quantity',
                    'sales.sell_price',
                ])
                ->leftJoin('stock_items', 'sales.stock_item_id', '=', 'stock_items.id')
                ->leftJoin('stock_categories', 'stock_items.stock_category_id', '=', 'stock_categories.id')
                ->groupBy('sales.id')
                ->get();

        $total_members = count($sales); // get your total no of data;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $sales,
        );

        return response()->json($data);
    }
}
