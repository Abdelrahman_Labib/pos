<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\StockItem;
use Illuminate\Http\Request;

class GetItemsAjaxDetails extends Controller
{
    public function __invoke(Request $request)
    {
        $items =
        StockItem::select('id', 'name', 'barcode', 'quantity', 'sell_price', 'purchase_price')
        ->where('name','like', "%{$request->search}%")
        ->orWhere('barcode','like', "%{$request->search}%")
        ->get();

        return response()->json($items);
    }
}
