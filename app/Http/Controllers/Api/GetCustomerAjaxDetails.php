<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class GetCustomerAjaxDetails extends Controller
{
    public function __invoke(Request $request)
    {
        $customers = Customer::select('id', 'name', 'phone')->where('phone','like', "%{$request->search}%")->get();

        return response()->json($customers);
    }
}
