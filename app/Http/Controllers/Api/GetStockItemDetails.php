<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetStockItemDetails extends Controller
{
    public function __invoke(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($filter['value']))? $filter['value'] : false;

        $items =
        DB::table('stock_items')
        ->select([
            'stock_items.id',
            'stock_categories.name as category_name',
            'stock_items.stock_category_id',
            'stock_items.name',
            'stock_items.barcode',
            'stock_items.sell_price',
            'stock_items.purchase_price',
            'stock_items.quantity',
            'stock_items.notification_limit',
            'stock_items.notes',
        ])
        ->leftJoin('stock_categories', 'stock_items.stock_category_id', '=', 'stock_categories.id')
        ->groupBy('stock_items.id')
        ->get();

        $total_members = count($items); // get your total no of data;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $items,
        );

        return response()->json($data);
    }
}
