<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetCustomers extends Controller
{
    public function __invoke(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($filter['value']))? $filter['value'] : false;

        $customers =
            DB::table('customers')
                ->select([
                    'customers.id',
                    'customers.name',
                    'customers.phone',
                ])
                ->groupBy('customers.id')
                ->take(10)
                ->get();

        $total_members = count($customers); // get your total no of data;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $customers,
        );

        return response()->json($data);
    }
}
