<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetPurchases extends Controller
{
    public function __invoke(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($filter['value']))? $filter['value'] : false;

        $purchases =
            DB::table('purchases')
                ->select([
                    'purchases.id',
                    'suppliers.id as supplier_id',
                    'suppliers.name as supplier_name',
                    'stock_items.name as item_name',
                    DB::raw("concat(stock_items.name , '/', stock_items.barcode) as stock_item"),
                    'purchases.quantity',
                    'purchases.purchase_price as purchase',
                    'purchases.date',
                ])
                ->leftJoin('suppliers', 'purchases.supplier_id', '=', 'suppliers.id')
                ->leftJoin('stock_items', 'purchases.stock_item_id', '=', 'stock_items.id')
                ->groupBy('suppliers.id')
                ->get();

        $total_members = count($purchases); // get your total no of data;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $purchases,
        );

        return response()->json($data);
    }
}
