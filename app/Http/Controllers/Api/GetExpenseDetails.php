<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetExpenseDetails extends Controller
{
    public function __invoke(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($filter['value']))? $filter['value'] : false;

        $expenses =
        DB::table('expenses')
        ->select([
            'expenses.id',
            'expense_categories.name as category_name',
            'expenses.expense_category_id',
            'expenses.name',
            'expenses.cost',
            'expenses.date',
            'expenses.notes',
        ])
        ->leftJoin('expense_categories', 'expenses.expense_category_id', '=', 'expense_categories.id')
        ->groupBy('expenses.id')
        ->get();

        $total_members = count($expenses); // get your total no of data;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $expenses,
        );

        return response()->json($data);
    }
}
