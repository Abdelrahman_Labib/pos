<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GetQuotations extends Controller
{
    public function __invoke(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($filter['value']))? $filter['value'] : false;

        $quotations =
            DB::table('quotations')
                ->select([
                    'quotations.id',
                    'stock_categories.name as category_name',
                    'stock_items.name as item_name',
                    'stock_items.barcode as item_barcode',
                    'stock_items.purchase_price',
                    'quotations.quantity',
                    'quotations.sell_price',
                ])
                ->leftJoin('stock_items', 'quotations.stock_item_id', '=', 'stock_items.id')
                ->leftJoin('stock_categories', 'stock_items.stock_category_id', '=', 'stock_categories.id')
                ->groupBy('quotations.id')
                ->get();

        $total_members = count($quotations); // get your total no of data;

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $quotations,
        );

        return response()->json($data);
    }
}
