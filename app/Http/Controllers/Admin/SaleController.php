<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerPayment;
use App\Models\CustomerSale;
use App\Models\Sale;
use App\Models\StockItem;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index()
    {
        return view('Admin.sales.index');
    }

    public function store(Request $request)
    {
        //VALIDATE REQUEST
        $request->validate([
            'customer_phone' => 'required|max:190',
            'customer_name'  => 'required|max:190',
            'item_array'     => 'required|array',
            'discount'       => 'sometimes',
            'total'          => 'required|numeric',
            'payments'       => 'sometimes|numeric',
        ]);

        //CHECK CUSTOMER EXISTENCE
        $customer = Customer::where('phone', $request->customer_phone)->select('id')->first();

        if (!$customer) {
            $customer = Customer::create([
                'name'  => $request->customer_name,
                'phone' => $request->customer_phone,
            ]);
        }

        //CREATE NEW CUSTOMER SALE COLUMN
        $customer_sale = CustomerSale::create([
            'customer_id' => $customer->id,
            'discount'    => $request->discount,
            'total'       => $request->total,
        ]);

        foreach ($request->item_array as $itemDetails) {

            //CHECK ITEM BARCODE EXISTENCE
            $barcode = substr($itemDetails['item_barcode'], strpos($itemDetails['item_barcode'], "/") + 1);
            $stock_item = StockItem::where('barcode', $barcode)->select('id', 'quantity')->first();

            //SUBTRACT SALE QUANTITY FROM ITEM QUANTITY
            $newQuantity = $stock_item->quantity - $itemDetails['quantity'];

            //CHECK QUANTITY ITEM
            if ($stock_item->quantity == 0 || $newQuantity < 0) {
                return 'invalidQuantity';
            }

            //CREATE NEW SALE COLUMN
            Sale::create([
                'customer_sale_id' => $customer_sale->id,
                'stock_item_id'    => $stock_item->id,
                'quantity'         => $itemDetails['quantity'],
                'sell_price'       => $itemDetails['sell_price'],
            ]);

            //UPDATE NEW QUANTITY
            $stock_item->update([
                'quantity' => $newQuantity,
            ]);
        }

        if ($request->payments != '') {
            CustomerPayment::create([
                'customer_sale_id' => $customer_sale->id,
                'paid'             => $request->payments >= $request->total ? $request->total : $request->payments,
                'remain'           => $request->payments >= $request->total ? 0 : $request->total - $request->payments,
            ]);
        }

        return ['status' => 'success', 'type' => 'sales' ,'invoice_id' => $customer_sale->id];
    }

    public function destroy($id)
    {
        //
    }
}
