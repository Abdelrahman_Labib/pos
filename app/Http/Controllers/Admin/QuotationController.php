<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerQuotation;
use App\Models\Quotation;
use App\Models\StockItem;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    public function index()
    {
        $customers = Customer::select('id', 'name', 'phone')->get();
        return view('Admin.quotations.index', compact('customers'));
    }

    public function store(Request $request)
    {
        //VALIDATE REQUEST
        $request->validate([
            'customer_phone' => 'required|max:190',
            'customer_name'  => 'required|max:190',
            'item_array'     => 'required|array',
            'discount'       => 'sometimes',
            'total'          => 'required|numeric',
        ]);

        //CHECK CUSTOMER EXISTENCE
        $customer = Customer::where('phone', $request->customer_phone)->select('id')->first();

        if (!$customer) {
            $customer = Customer::create([
                'name'  => $request->customer_name,
                'phone' => $request->customer_phone,
            ]);
        }

        $customer_quotation = CustomerQuotation::create([
            'customer_id'  => $customer->id,
            'discount'     => $request->discount,
            'total'        => $request->total,
        ]);

        //LOOP REQUEST
        foreach ($request->item_array as $itemDetails) {

            //CHECK ITEM BARCODE EXISTENCE
            $barcode = substr($itemDetails['item_barcode'], strpos($itemDetails['item_barcode'], "/") + 1);
            $stock_item = StockItem::where('barcode', $barcode)->select('id')->first();

            if(!$stock_item){
                return false;
            }

            //CREATE NEW QUOTATION COLUMN
            Quotation::create([
                'customer_quotation_id' => $customer_quotation->id,
                'stock_item_id'         => $stock_item->id,
                'quantity'              => $itemDetails['quantity'],
                'sell_price'            => $itemDetails['sell_price'],
            ]);
        }

        return ['status' => 'success', 'type' => 'quotations', 'invoice_id' => $customer_quotation->id];
    }

    public function destroy($id)
    {
        Quotation::where('id', $id)->delete();

        return back()->with('success', __('admin.deleteSuccessMessage'));
    }
}
