<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\ExpenseCategory;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    public function index()
    {
        $categories = ExpenseCategory::select('id', 'name')->get();

        return view('Admin.expenses.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $validate = $this->checkValidation($request);

        Expense::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage') );
    }

    public function update(Request $request, $id)
    {
        $validate = $this->checkValidation($request, $id);

        Expense::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        Expense::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'expense_category_id' => 'required|exists:expense_categories,id',
            'name'                => 'required|max:190',
            'cost'                => 'required|numeric',
            'date'                => 'required|date',
            'notes'               => 'required|max:350',
        ]);
    }
}
