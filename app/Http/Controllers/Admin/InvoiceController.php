<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public function index($type, $id)
    {
        if($type == 'sales'){

            $invoice =
            DB::table('customer_sales')
            ->select([
                'customer_sales.id',
                'customers.name',
                'customers.phone',
                DB::raw('customer_sales.total + customer_sales.discount as sub_total'),
                'customer_sales.discount',
                'customer_sales.total',
                'customer_sales.created_at',
                'customer_payments.paid',
            ])
            ->join('customers', 'customer_sales.customer_id', '=', 'customers.id')
            ->join('customer_payments', 'customer_sales.id', '=', 'customer_payments.customer_sale_id')
            ->where('customer_sales.id', $id)
            ->first();

            $items =
            DB::table('sales')
            ->select([
                'sales.id',
                DB::raw("concat(stock_items.name , '/', stock_items.barcode) as stock_item"),
                'sales.sell_price',
                'sales.quantity',
                DB::raw('sales.sell_price * sales.quantity as sub_total'),
            ])
            ->join('stock_items', 'sales.stock_item_id', '=', 'stock_items.id')
            ->where('sales.customer_sale_id', $id)
            ->get();
        }else{

            $invoice =
                DB::table('customer_quotations')
                ->select([
                    'customer_quotations.id',
                    'customers.name',
                    'customers.phone',
                    DB::raw('customer_quotations.total + customer_quotations.discount as sub_total'),
                    'customer_quotations.discount',
                    'customer_quotations.total',
                    'customer_quotations.created_at',
                ])
                ->join('customers', 'customer_quotations.customer_id', '=', 'customers.id')
                ->where('customer_quotations.id', $id)
                ->first();

            $items =
                DB::table('quotations')
                ->select([
                    'quotations.id',
                    DB::raw("concat(stock_items.name , '/', stock_items.barcode) as stock_item"),
                    'quotations.sell_price',
                    'quotations.quantity',
                    DB::raw('quotations.sell_price * quotations.quantity as sub_total'),
                ])
                ->join('stock_items', 'quotations.stock_item_id', '=', 'stock_items.id')
                ->where('quotations.customer_quotation_id', $id)
                ->get();
        }
        return view('Admin.invoices.index', compact('invoice', 'items'));
    }
}
