<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Purchase;
use App\Models\StockItem;
use App\Models\Supplier;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::select('id', 'name')->get();

        return view('Admin.purchases.index', compact('suppliers'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'supplier_id'     => 'required|exists:suppliers,id',
            'stock_item_id'   => 'required|array',
//            "stock_item_id.*" => 'required|exists:stock_items,barcode,' . $barcode,
            'quantity'        => 'required|array',
            "quantity.*"      => 'required|max:190|numeric',
            'purchase'        => 'required|array',
            "purchase.*"      => 'required|max:190|numeric',
            'date'            => 'required|date',
        ]);

        for($i = 0; $i < count($request->stock_item_id); $i++){
            $barcode = substr($request->stock_item_id[$i], strpos($request->stock_item_id[$i], "/") + 1);
            $stock_item = StockItem::where('barcode', $barcode)->select('id')->first();

            if($stock_item){
                Purchase::create([
                    'supplier_id'    => $request->supplier_id,
                    'stock_item_id'  => $stock_item->id,
                    'quantity'       => $request->quantity[$i],
                    'purchase_price' => $request->purchase[$i],
                    'date'           => $request->date,
                ]);

                StockItem::where('id', $stock_item->id)->update([
                    'quantity'       => $request->quantity[$i],
                    'purchase_price' => $request->purchase[$i],
                ]);
            }
        }

        return back()->with('success', __('admin.storeSuccessMessage') );
    }

    public function destroy($id)
    {
        Purchase::where('supplier_id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }
}
