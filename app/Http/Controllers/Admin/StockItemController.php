<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StockCategory;
use App\Models\StockItem;
use Illuminate\Http\Request;

class StockItemController extends Controller
{
    public function index()
    {
        $categories = StockCategory::select('id', 'name')->get();

        return view('Admin.stock.items.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $validate = $this->checkValidation($request);

        StockItem::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage') );
    }

    public function update(Request $request, $id)
    {
        $validate = $this->checkValidation($request, $id);

        StockItem::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        StockItem::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'stock_category_id'  => 'required|exists:stock_categories,id',
            'name'               => 'required|max:190|unique:stock_items,name,'.$id.',id',
            'barcode'            => 'required|max:190|unique:stock_items,barcode,'.$id.',id',
            'sell_price'         => 'required|numeric',
            'purchase_price'     => 'required|numeric',
            'quantity'           => 'required|numeric',
            'notification_limit' => 'required|numeric',
            'notes'              => 'required|max:350',
        ]);
    }
}
