<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExpenseCategory;
use Illuminate\Http\Request;

class ExpenseCategoryController extends Controller
{
    public function index()
    {
        $categories = ExpenseCategory::select('id', 'name')->get();

        return view('Admin.expenses.categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $validate = $this->checkValidation($request);

        ExpenseCategory::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $id)
    {
        $validate = $this->checkValidation($request, $id);

        ExpenseCategory::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        ExpenseCategory::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name' => 'required|max:190|unique:expense_categories,name,'.$id.',id',
        ]);
    }
}
