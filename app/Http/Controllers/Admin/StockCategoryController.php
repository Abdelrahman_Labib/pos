<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StockCategory;
use Illuminate\Http\Request;

class StockCategoryController extends Controller
{
    public function index()
    {
        $categories = StockCategory::select('id', 'name')->get();

        return view('Admin.stock.categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $validate = $this->checkValidation($request);

        StockCategory::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $id)
    {
        $validate = $this->checkValidation($request, $id);

        StockCategory::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        StockCategory::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name' => 'required|max:190|unique:stock_categories,name,'.$id.',id,deleted_at,NULL',
        ]);
    }
}
