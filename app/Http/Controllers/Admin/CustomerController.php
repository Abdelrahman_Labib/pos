<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        return view('Admin.customers.index');
    }

    public function store(Request $request)
    {
        $validate = $this->checkValidation($request);

        Customer::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage'));
    }

    public function update(Request $request, $id)
    {
        $validate = $this->checkValidation($request, $id);

        Customer::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        Customer::where('id', $id)->delete();

        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name'  => 'required|max:190',
            'phone' => 'required|max:190|unique:customers,phone,'.$id.',id',
        ]);
    }
}
