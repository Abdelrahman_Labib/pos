<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        return view('Admin.suppliers.index');
    }

    public function store(Request $request)
    {
        $validate = $this->checkValidation($request);

        Supplier::create($validate);

        return back()->with('success', __('admin.storeSuccessMessage') );
    }

    public function update(Request $request, $id)
    {
        $validate = $this->checkValidation($request, $id);

        Supplier::where('id', $id)->update($validate);

        return back()->with('success', __('admin.updateSuccessMessage'));
    }

    public function destroy($id)
    {
        Supplier::where('id', $id)->delete();
        return back()->with('success', __('admin.deleteSuccessMessage'));
    }

    public function checkValidation($input, $id = null)
    {
        return $input->validate([
            'name'    => 'required|max:190|unique:suppliers,name,'.$id.',id',
            'contact' => 'required|max:190',
            'address' => 'required|max:190',
            'notes'   => 'required|max:350',
        ]);
    }
}
