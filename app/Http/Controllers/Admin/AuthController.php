<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function viewLoginPage()
    {
        if(Auth::guard('admin')->user())
        {
            return redirect()->route('admin.dashboard');
        }

        return view('Admin.login.index');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'phone'     => 'required',
            'password'  => 'required|min:6'
        ]);

        if(Auth::guard('admin')->attempt(['phone' => $request->phone, 'password' => $request->password, 'active' => 1], false))
        {
            return redirect()->route('admin.dashboard');
        }else{
            return back()->with('error', 'Invalid Credentials');
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
